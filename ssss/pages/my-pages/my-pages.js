var app = getApp();
Page({
  data: {
    background: ['demo-text-1', 'demo-text-2', 'demo-text-3'],
    indicatorDots: true,
    vertical: false,
    autoplay: false,
    interval: 2000,
    duration: 500,
    list: []
  },

  onShow: function (e) {
    var MemberInfo = wx.getStorageSync('MemberInfo');
    var that = this;
    wx.showLoading();
    wx.request({
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'Authorization': app.Getopenid()
      },
      method: 'POST',
      url: app.DOMAIN_NAME + '/wechat/wechatFamily/listByUser',
      success: res => {
        if (res.data.status == 10000) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '身份已过期,需重新授权',
            success: res => {
              if (res.confirm) {
                wx.navigateTo({
                  url: '/pages/login/login',
                })
              }
            }
          });
        } else if (res.data.status != 200 || !res.data.status) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '服务器开小差去了，请重试',
            showCancel: false
          });
        } else if (res.data.status == 200) {
          wx.hideLoading()
          that.setData({
            list: res.data.data,
            MemberInfo
          })
        }
      }
    })
  },

  compile: function(e) {
    var patientId = e.currentTarget.dataset.patientid;
    wx.navigateTo({
      url: '/pages/compile/compile?familyid=' + patientId
    })
  },

  addmember: function() {
    wx.navigateTo({
      url: '/pages/information/information',
    })
  },

  gotmember: function() {
    wx.navigateTo({
      url: '/pages/member/member',
    })
  },

  examine: function() {
    wx.navigateTo({
      url: '/pages/detection/detection',
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})