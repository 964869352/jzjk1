// pages/photos/photos.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    if (e.shili){
      wx.setNavigationBarTitle({
        title: '动作示例',
      })
    } else if (e.immg) {
      const stype = e.immg
      const type = stype.substring(stype.length - 3);
      if (type == 'mp4') {
        wx.setNavigationBarTitle({
          title: '视频预览',
        })
        this.setData({v:111})
      } else {
        wx.setNavigationBarTitle({
          title: '照片预览',
        })
        this.setData({ i: 111 })
      }
    }
    this.setData({
      wws: e.immg,
      index: e.index,
      shili:e.shili
    })
    console.log(this.data.index)
  },
  rephotograph: function() {
    var img_url = wx.getStorageSync('img_url'),
        index = this.data.index
    wx.showModal({
      title: '提示',
      content: '确认删除吗？',
      success(res) {
        if (res.confirm) {
          img_url[index] = ''
          wx.setStorageSync('img_url', img_url)
          setTimeout(function () {
            wx.showToast({
              title: '删除成功',
              icon:'success'
            })
          }, 500)
          wx.navigateBack({})
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  next: function() {
    wx.navigateBack({})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})