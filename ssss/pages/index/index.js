const app = getApp()
Page({
  data: {
    list:[
      { state: 0, text: '预防脊柱侧弯', content: '什么是脊柱侧弯？很严重？家长应该如何预防孩子侧弯？', time: '2019-11-12' },
      { state: 1, text: '脊柱侧弯的危害', content: '脊柱侧弯的危害有多大? 告诉你真相:真的伤不起!', time: '2019-11-12' }
    ]
  },
  onLoad:function(){
    var mobile = wx.getStorageSync('mobile');
    this.setData({
      mobile
    })
  },
  navtodetail:e=>{
    console.log(e.currentTarget.dataset.inde)
    wx.navigateTo({
      url: '/pages/detail/detail?index=' + e.currentTarget.dataset.inde,
    })
  },
  deatil:function(e){
    wx.navigateTo({
      url: '/pages/test/test?type=' + e.currentTarget.dataset.type,
    })
  }
})