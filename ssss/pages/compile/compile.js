var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sex: ['男', '女'],
    type: ['爱人', '父亲', '母亲', '爷爷', '奶奶', '儿子', '女儿', '其他'],
    sexindex: 1,
    typeindex: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    var that = this;
    that.setData({ familyid: e.familyid})
    console.log(e.familyid)
    if (!e.familyid) return false;
    wx.showLoading();
    wx.request({
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'content-type': 'application/json',
        'Authorization': app.Getopenid()
      },
      method: 'GET',
      url: app.DOMAIN_NAME +'/wechat/wechatFamily/familyInfo',
      data: {
        familyId: e.familyid
      },
      success: res => {
        if (res.data.status == 10000) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '身份已过期,需重新授权',
            success: res => {
              if (res.confirm) {
                wx.navigateTo({
                  url: '/pages/login/login',
                })
              }
            }
          });
        } else if (res.data.status != 200 || !res.data.status) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '服务器开小差去了，请重试',
            showCancel: false
          });
        } else if (res.data.status == 200) {
          wx.hideLoading()
          that.setData({ info: res.data.data, sexindex: res.data.data.sex, typeindex: res.data.data.type })
        }
      }
    })
  },

  // 关系选择
  typeChange: function(e) {
    this.setData({
      typeindex: e.detail.value
    })
  },

  // 关系选择
  sexChange: function(e) {
    this.setData({
      sexindex: e.detail.value
    })
  },

  newSubmit: function(e) {
    var that = this;
    var token = wx.getStorageSync('token');
    var deta = e.detail.value,
        birthday = deta.birthday,
        allergic = deta.allergic,
        habit = deta.habit,
        height = deta.height,
        idCard = deta.idCard,
        medicalHistory = deta.medicalHistory,
        mobile = deta.mobile,
        name = deta.name,
        sex = deta.sex,
        type = deta.type,
        weight = deta.weight
      wx.showLoading({
        title: "正在保存",
        mask: true
      });
    wx.request({
      header: {
        'content-type': 'application/json',
        'Authorization': token
      },
      method: 'POST',
      url: app.DOMAIN_NAME +'/wechat/wechatFamily/update',
      data: {
        birthday,
        allergic,
        habit,
        height,
        idCard,
        medicalHistory,
        mobile,
        name,
        sex,
        type,
        weight,
        id: that.data.familyid
      },
      success: res => {
        if (res.data.status == 10000) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '身份已过期,需重新授权',
            success: res => {
              if (res.confirm) {
                wx.navigateTo({
                  url: '/pages/login/login',
                })
              }
            }
          });
        } else if (res.data.status != 200 || !res.data.status) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '服务器开小差去了，请重试',
            showCancel: false
          });
        } else if (res.data.status == 200) {
          wx.hideLoading()
          wx.navigateBack()
        }
      }
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})