const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ehide: 1, //动作示例
    uhide: 1, //上传图片
    videohide: 1,
    img_url: [],
    video_url: [],
    vhide: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    console.log(e)
    this.setData({
      userid: e.userid
    })
  },
  back: function() {
    this.setData({
      uhide: 1
    })
  },

  imageSubmit: function(e) {
    let that = this;
    let img_url = that.data.img_url;
    let video_url = that.data.video_url;
    let img_url_ok = [];
    var id = that.data.userid
    if (img_url == '' && video_url == '') {
      wx.showToast({
        title: '请先添加资料',
        icon: 'none'
      });
      return false
    }
    wx.showLoading({
      title: '上传中',
    })
    if (img_url != '' && video_url != ''){
      that.upiav()
    } else if (img_url=='' && video_url){
      that.upvideo()
    } else if (img_url && video_url == '') {
      that.upimga()
    }
    
  },
  upimga: function (e) {
    let that = this;
    let img_url = that.data.img_url;
    let video_url = that.data.video_url;
    let img_url_ok = [];
    var id = that.data.userid
    //图片上传
    for (let i = 0; i < img_url.length; i++) {
      console.log(img_url)
      wx.uploadFile({
        //路径填你上传图片方法的地址
        url: app.DOMAIN_NAME + '/wechat/analysis/upload',
        header: {
          'content-type': 'application/json',
          'content-type': 'application/x-www-form-urlencoded',
          'Authorization': app.Getopenid()
        },
        filePath: img_url[i],
        name: 'file',
        formData: {
          file: JSON.stringify(img_url),
          patientId: id,
          fileType: 'IMAGE'
        },
        success: function (res) {
          var deta = JSON.parse(res.data)
          if (deta.status == 10000) {
            wx.hideLoading()
            wx.showModal({
              confirmText: '好的',
              content: deta.errmsg || '身份已过期,需重新授权',
              success: res => {
                if (res.confirm) {
                  wx.navigateTo({
                    url: '/pages/login/login',
                  })
                }
              }
            });
          } else if (deta.status != 200 || !deta.status) {
            wx.hideLoading()
            wx.showModal({
              confirmText: '好的',
              content: deta.errmsg || '服务器开小差去了，请重试',
              showCancel: false
            });
          } else if (deta.status == 200) {
            wx.hideLoading()
            wx.showToast({
              title: '上传成功',
              success: function () {
                setTimeout(function () {
                  wx.reLaunch({
                    url: '/pages/record/record?userid=' + that.data.userid,
                  })
                }, 1000);
              }
            })
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '上传失败',
          })
        }
      })
    }
  },
  upvideo: function () {
    let that = this;
    let img_url = that.data.img_url;
    let video_url = that.data.video_url;
    let img_url_ok = [];
    var id = that.data.userid
    //视频上传
    for (let i = 0; i < video_url.length; i++) {
      wx.uploadFile({
        header: {
          'content-type': 'application/json',
          'content-type': 'application/x-www-form-urlencoded',
          'Authorization': app.Getopenid()
        },
        url: app.DOMAIN_NAME + '/wechat/analysis/upload',
        filePath: video_url[i],
        name: 'file',
        formData: {
          file: JSON.stringify(video_url),
          fileType: 'MEDIA',
          patientId: id
        },
        success: function (res) {
          var deta = JSON.parse(res.data)
          if (deta.status == 10000) {
            wx.hideLoading()
            wx.showModal({
              confirmText: '好的',
              content: deta.errmsg || '身份已过期,需重新授权',
              success: res => {
                if (res.confirm) {
                  wx.navigateTo({
                    url: '/pages/login/login',
                  })
                }
              }
            });
          } else if (deta.status != 200 || !deta.status) {
            wx.hideLoading()
            wx.showModal({
              confirmText: '好的',
              content: deta.errmsg || '服务器开小差去了，请重试',
              showCancel: false
            });
          } else if (deta.status == 200) {
            wx.hideLoading()
            wx.showToast({
              title: '上传成功',
              success: function () {
                setTimeout(function () {
                  wx.reLaunch({
                    url: '/pages/record/record?userid=' + that.data.userid,
                  })
                }, 1000);
              }
            })
          }
        },
        fail: function (res) { }
      })
    }
  },
  upiav: function () {
    let that = this;
    let img_url = that.data.img_url;
    let video_url = that.data.video_url;
    let img_url_ok = [];
    var id = that.data.userid
    //视频上传
    for (let i = 0; i < video_url.length; i++) {
      wx.uploadFile({
        header: {
          'content-type': 'application/json',
          'content-type': 'application/x-www-form-urlencoded',
          'Authorization': app.Getopenid()
        },
        url: app.DOMAIN_NAME + '/wechat/analysis/upload',
        // https://aabbcc.thinkine.com/wechat/analysis/upload
        filePath: video_url[i],
        name: 'file',
        formData: {
          file: JSON.stringify(video_url),
          fileType: 'MEDIA',
          patientId: id
        },
        success: function (res) {
          //图片上传
          for (let i = 0; i < img_url.length; i++) {
            wx.uploadFile({
              //路径填你上传图片方法的地址
              url: app.DOMAIN_NAME + '/wechat/analysis/upload',
              header: {
                'content-type': 'application/json',
                'content-type': 'application/x-www-form-urlencoded',
                'Authorization': app.Getopenid()
              },
              filePath: img_url[i],
              name: 'file',
              formData: {
                file: JSON.stringify(img_url),
                patientId: id,
                fileType: 'IMAGE'
              },
              success: function (res) {
                var deta = JSON.parse(res.data)
                if (deta.status == 10000) {
                  wx.hideLoading()
                  wx.showModal({
                    confirmText: '好的',
                    content: deta.errmsg || '身份已过期,需重新授权',
                    success: res => {
                      if (res.confirm) {
                        wx.navigateTo({
                          url: '/pages/login/login',
                        })
                      }
                    }
                  });
                } else if (deta.status != 200 || !deta.status) {
                  wx.hideLoading()
                  wx.showModal({
                    confirmText: '好的',
                    content: deta.errmsg || '服务器开小差去了，请重试',
                    showCancel: false
                  });
                } else if (deta.status == 200) {
                  wx.hideLoading()
                  wx.showToast({
                    title: '上传成功',
                    success: function () {
                      setTimeout(function () {
                        wx.reLaunch({
                          url: '/pages/record/record?userid=' + that.data.userid,
                        })
                      }, 1000);
                    }
                  })
                }
              },
              fail: function (res) {
                wx.showToast({
                  title: '上传失败',
                })
              }
            })
          }
        },
        fail: function (res) { }
      })
    }
  },
  example: function() {
    this.setData({
      ehide: 2
    })
  },
  chooseimage: function() {
    var that = this;
    wx.chooseImage({
      count: 5, // 默认9  
      success: function(res) {
        wx.setNavigationBarTitle({
          title: '上传照片',
        })
        if (res.tempFilePaths.length > 0) {
          //图如果满了9张，不显示加图
          if (res.tempFilePaths.length == 5) {
            that.setData({
              hideAdd: 1
            })
          } else {
            that.setData({
              hideAdd: 0
            })
          }
          //把每次选择的图push进数组
          let img_url = that.data.img_url;
          for (let i = 0; i < res.tempFilePaths.length; i++) {
            img_url.push(res.tempFilePaths[i])
          }
          that.setData({
            img_url: img_url,
            uhide: 2
          })
          if (that.data.img_url.length == 5) {
            that.setData({
              hideAdd: 1
            })
          }
        }
      }
    })
  },
  preview: function(e) {
    var i = e.currentTarget.dataset.ind;
    wx.previewImage({
      current: this.data.img_url[i], // 当前显示图片的http链接
      urls: this.data.img_url // 需要预览的图片http链接列表
    })
  },
  // 上传视频
  choosevideo() {
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      compressed: true,
      maxDuration: 10,
      camera: 'back',
      success: res => {
        const video = res.tempFilePath;
        //把每次选择的图push进数组
        let video_url = this.data.video_url;
        video_url.push(video)
        this.setData({
          video_url: video_url,
          uhide: 3
        })
      }
    })
  },
  getvideo: function() {
    this.setData({
      uhide: 3
    })
  },
  getimage: function() {
    this.setData({
      uhide: 2
    })
  },
  playvideo: function(e) {
    console.log(e.currentTarget.dataset.vurl)
    this.setData({
      playurl: e.currentTarget.dataset.vurl
    })
  },
  upimga: function (e) {
    console.log(333)
    let that = this;
    let img_url = that.data.img_url;
    let video_url = that.data.video_url;
    let img_url_ok = [];
    var id = that.data.userid
    //图片上传
    for (let i = 0; i < img_url.length; i++) {
      wx.uploadFile({
        //路径填你上传图片方法的地址
        url: app.DOMAIN_NAME + '/wechat/analysis/upload',
        header: {
          'content-type': 'application/json',
          'content-type': 'application/x-www-form-urlencoded',
          'Authorization': app.Getopenid()
        },
        filePath: img_url[i],
        name: 'file',
        formData: {
          file: JSON.stringify(img_url),
          patientId: id,
          fileType: 'IMAGE'
        },
        success: function(res) {
          var deta = JSON.parse(res.data)
          if (deta.status == 10000) {
            wx.hideLoading()
            wx.showModal({
              confirmText: '好的',
              content: deta.errmsg || '身份已过期,需重新授权',
              success: res => {
                if (res.confirm) {
                  wx.navigateTo({
                    url: '/pages/login/login',
                  })
                }
              }
            });
          } else if (deta.status != 200 || !deta.status) {
            wx.hideLoading()
            wx.showModal({
              confirmText: '好的',
              content: deta.errmsg || '服务器开小差去了，请重试',
              showCancel: false
            });
          } else if (deta.status == 200) {
            wx.hideLoading()
            wx.showToast({
              title: '上传成功',
              success: function() {
                setTimeout(function() {
                  wx.navigateTo({
                    url: '/pages/record/record?userid=' + that.data.userid,
                  })
                }, 1000);
              }
            })
          }
        },
        fail: function(res) {
          wx.showToast({
            title: '上传失败',
          })
        }
      })
    }
  },
  upvideo: function() {
    let that = this;
    let img_url = that.data.img_url;
    let video_url = that.data.video_url;
    let img_url_ok = [];
    var id = that.data.userid
    //视频上传
    for (let i = 0; i < video_url.length; i++) {
      wx.uploadFile({
        header: {
          'content-type': 'application/json',
          'content-type': 'application/x-www-form-urlencoded',
          'Authorization': app.Getopenid()
        },
        url: app.DOMAIN_NAME + '/wechat/analysis/upload',
        filePath: video_url[i],
        name: 'file',
        formData: {
          file: JSON.stringify(video_url),
          fileType: 'MEDIA',
          patientId: id
        },
        success: function(res) {
          var deta = JSON.parse(res.data)
          if (deta.status == 10000) {
            wx.hideLoading()
            wx.showModal({
              confirmText: '好的',
              content: deta.errmsg || '身份已过期,需重新授权',
              success: res => {
                if (res.confirm) {
                  wx.navigateTo({
                    url: '/pages/login/login',
                  })
                }
              }
            });
          } else if (deta.status != 200 || !deta.status) {
            wx.hideLoading()
            wx.showModal({
              confirmText: '好的',
              content: deta.errmsg || '服务器开小差去了，请重试',
              showCancel: false
            });
          } else if (deta.status == 200) {
            wx.hideLoading()
            wx.showToast({
              title: '上传成功',
              success: function() {
                setTimeout(function() {
                  wx.navigateTo({
                    url: '/pages/record/record?userid=' + that.data.userid,
                  })
                }, 1000);
              }
            })
          }
        },
        fail: function(res) {}
      })
    }
  },
  upiav: function() {
    let that = this;
    let img_url = that.data.img_url;
    let video_url = that.data.video_url;
    let img_url_ok = [];
    var id = that.data.userid
    //视频上传
    for (let i = 0; i < video_url.length; i++) {
      wx.uploadFile({
        header: {
          'content-type': 'application/json',
          'content-type': 'application/x-www-form-urlencoded',
          'Authorization': app.Getopenid()
        },
        url: app.DOMAIN_NAME + '/wechat/analysis/upload',
        // https://aabbcc.thinkine.com/wechat/analysis/upload
        filePath: video_url[i],
        name: 'file',
        formData: {
          file: JSON.stringify(video_url),
          fileType: 'MEDIA',
          patientId: id
        },
        success: function(res) {
          //图片上传
          for (let i = 0; i < img_url.length; i++) {
            wx.uploadFile({
              //路径填你上传图片方法的地址
              url: app.DOMAIN_NAME + '/wechat/analysis/upload',
              header: {
                'content-type': 'application/json',
                'content-type': 'application/x-www-form-urlencoded',
                'Authorization': app.Getopenid()
              },
              filePath: img_url[i],
              name: 'file',
              formData: {
                file: JSON.stringify(img_url),
                patientId: id,
                fileType: 'IMAGE'
              },
              success: function(res) {
                var deta = JSON.parse(res.data)
                if (deta.status == 10000) {
                  wx.hideLoading()
                  wx.showModal({
                    confirmText: '好的',
                    content: deta.errmsg || '身份已过期,需重新授权',
                    success: res => {
                      if (res.confirm) {
                        wx.navigateTo({
                          url: '/pages/login/login',
                        })
                      }
                    }
                  });
                } else if (deta.status != 200 || !deta.status) {
                  wx.hideLoading()
                  wx.showModal({
                    confirmText: '好的',
                    content: deta.errmsg || '服务器开小差去了，请重试',
                    showCancel: false
                  });
                } else if (deta.status == 200) {
                  wx.hideLoading()
                  wx.showToast({
                    title: '上传成功',
                    success: function() {
                      setTimeout(function() {
                        wx.navigateTo({
                          url: '/pages/record/record?userid=' + that.data.userid,
                        })
                      }, 1000);
                    }
                  })
                }
              },
              fail: function(res) {
                wx.showToast({
                  title: '上传失败',
                })
              }
            })
          }
        },
        fail: function(res) {}
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})