const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    img_url: [
      '',
      '',
      '',
      '',
      ''
    ],
    src: [
      '/images/u.png',
      '/images/back.png',
      '/images/left.png',
      '/images/right.png',
      '/images/backh.png'
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    var img = wx.getStorageSync('img_url');
    if (!img){
      wx.setStorageSync('img_url', this.data.img_url)
    }
    console.log(img)
      this.setData({
        userid: e.userid
      })
  },
  shili:function(){
    wx.navigateTo({
      url: '/pages/photos/photos?shili=1',
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function(e) {
    var img = wx.getStorageSync('img_url');
    var img_url = this.data.img_url;
    this.setData({
      img_url:img
    })
    console.log(this.data.img_url)
  },
  navcamera: function(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    if (that.data.img_url[index]) {
      wx.navigateTo({
        url: '/pages/photos/photos?immg=' + that.data.img_url[index] + '&index=' + index,
      })
      return false
    };
    wx.navigateTo({
      url: '/pages/camera/camera',
      events: {
        // 为指定事件添加一个监听器，获取被打开页面传送到当前页面的数据
        acceptDataFromOpenedPage: function(e) {
          var imgurl = that.data.img_url
          imgurl[e.index] = e.src;
          that.setData({
            img_url: imgurl
          })
          console.log(e)
        },
        someEvent: function(data) {
          console.log(data)
        }
      },
      success: function(res) {
        // 通过eventChannel向被打开页面传送数据
        res.eventChannel.emit('acceptDataFromOpenerPage', {
          index: e.currentTarget.dataset.index,
          src: that.data.img_url[index]
        })
      }
    })
  },
  next:function(){
    let k = 0;
    let that = this;
    let img_url = that.data.img_url;
    for (let i = 0; i < img_url.length; i++){
      if (img_url[i]!=''){
        k++
      }
    }
    if (img_url[0] == '' || img_url[1] == ''){
      wx.showToast({
        title: '请先添加完照片',
        icon:'none'
      })
    } else {
      wx.showLoading({
        title: '上传中',
      })
      that.upimga()
    }
    console.log(k)
  },
  upimga: function(e) {
    let that = this;
    let img_url = that.data.img_url;
    let img_url_ok = [];
    let k = 0;
    let img_urla = [ '', '', '', '', '' ];
    var id = that.data.userid
    //图片上传
    for (let i = 0; i < img_url.length; i++) {
      const type = img_url[i].substring(img_url[i].length - 3) == 'mp4' ? 'MEDIA' :'IMAGE';
      console.log(img_url)
      wx.uploadFile({
        //路径填你上传图片方法的地址
        url: app.DOMAIN_NAME + '/wechat/analysis/upload',
        header: {
          'content-type': 'application/json',
          'content-type': 'application/x-www-form-urlencoded',
          'Authorization': app.Getopenid()
        },
        filePath: img_url[i],
        name: 'file',
        formData: {
          file: JSON.stringify(img_url),
          patientId: id,
          fileType: type
        },
        success: function(res) {
          console.log(res)
          k++
          if (k == 2) {
            var deta = JSON.parse(res.data)
            if (deta.status == 10000) {
              wx.hideLoading()
              wx.showModal({
                confirmText: '好的',
                content: deta.errmsg || '身份已过期,需重新授权',
                success: res => {
                  if (res.confirm) {
                    wx.navigateTo({
                      url: '/pages/login/login',
                    })
                  }
                }
              });
            } else if (deta.status != 200 || !deta.status) {
              wx.hideLoading()
              wx.showModal({
                confirmText: '好的',
                content: deta.errmsg || '服务器开小差去了，请重试',
                showCancel: false
              });
            } else if (deta.status == 200) {
              wx.hideLoading()
              wx.showToast({
                title: '上传成功',
                success: function () {
                  setTimeout(function () {
                    wx.reLaunch({
                      url: '/pages/record/record?userid=' + that.data.userid,
                    })
                    wx.setStorageSync('img_url', img_urla)
                  }, 1000);
                }
              })
            }
          }
        },
        fail: function(res) {
          // wx.showToast({
          //   title: '上传失败',
          // })
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})