let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 授权登录
   */
  authorLogin: function(e) {
    let _this = this;
    if (e.detail.errMsg !== 'getUserInfo:ok') {
      return false;
    }
    wx.showLoading({
      title: "正在登录",
      mask: true
    });
    // 执行微信登录
    wx.login({
      success: function(res) {
        //发送请求
        wx.request({
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          url: app.DOMAIN_NAME+'/social_user_login/login',
          data: {
            code: res.code,
            rawData: e.detail.rawData,
            encryptedData: e.detail.encryptedData,
            ivStr: e.detail.iv,
            signature: e.detail.signature
          },
          success: function (ret) {
            console.log(ret.data.data.token)
            if (ret.data.status != 200 || !ret.data.status) {
              wx.showModal({
                confirmText: '好的',
                content: ret.data.errmsg || '服务器开小差去了，请重试',
                showCancel: false
              });
            } else if (ret.data.status == 200) {
              wx.setStorageSync('MemberInfo', e.detail)
              wx.setStorageSync('token', ret.data.data.token)
              wx.setStorageSync('familyInfo', ret.data.data.familyInfo)
              wx.hideLoading()
              wx.reLaunch({
                url: '/pages/index/index',
              })
            }
          },
          fail: function(ret) {}
        })
      }
    });
  },

  /**
   * 授权成功 跳转回原页面
   */
  navigateBack: function() {
    wx.navigateBack();
    // let currentPage = wx.getStorageSync('currentPage');
    // wx.redirectTo({
    //   url: '/' + currentPage.route + '?' + App.urlEncode(currentPage.options)
    // });
  },

})