var app = getApp();
Page({
  data: {
    a:1,
    getCodeTime: 10, //发送短信的间隔时间
    verifyCodeTime: '获取验证码'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.setNavigationBarTitle({
      title: '绑定手机',
    })
    //限制发送验证码的时间，防止恶意刷短信
    var getCodeTime = wx.getStorageSync('getCodeTime');
    if (getCodeTime <= that.data.getCodeTime - 1 && getCodeTime > 0) {
      that.Countdown(that);
    } else {
      wx.setStorageSync('getCodeTime', that.data.getCodeTime);
      that.setData({ buttonDisable: false });
    }
  },

  getphone: function (e) {
    this.setData({ mobile: e.detail.value})
  },

  /**
     * 执行发送验证码
     */
  verifyCodeEvent: function (e) {
    var that = this;
    //验证手机号
    if (that.data.buttonDisable == true) return false;
    var mobile = that.data.mobile;
    var regMobile = /^[1]([3-9])[0-9]{9}$/;
    if (!regMobile.test(mobile)) {
      wx.showToast({ title: '手机号错误', duration: 3000, icon: 'none' });
      that.setData({ mobilefocus: true });
      return false;
    }
    //验证手机号 END
    wx.showLoading();
    wx.request({
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'GET',
      url: app.DOMAIN_NAME +'/sms/send',
      data: {
        username: mobile,
        type: 'OTHER'
      },
      success: res => {
        var mes = res.data.message,
            code = mes.slice(11, 15)
        that.setData({
          code
        })
        wx.hideLoading()
        that.Countdown();
      }
    })
  },

  /**
   * 发送短信倒计时,防止恶意刷短信
   */
  Countdown: function () {
    var that = this;
    that.setData({ buttonDisable: true });
    var getCodeTime = wx.getStorageSync('getCodeTime');
    var intervalId = setInterval(function () {
      getCodeTime--;
      wx.setStorageSync('getCodeTime', getCodeTime);
      that.setData({
        verifyCodeTime: getCodeTime + 's后重发'
      });
      //倒计时完成
      if (getCodeTime == 0) {
        wx.setStorageSync('getCodeTime', that.data.getCodeTime);
        clearInterval(intervalId);
        that.setData({
          verifyCodeTime: '获取验证码',
          buttonDisable: false
        })
      }
    }, 1000);
  },

  bSubmit: function (e) {
    var token = wx.getStorageSync('token');
    var phone = e.detail.value.phone,
        code = e.detail.value.code,
        password = e.detail.value.password,
        regMobile = /^1(3|4|5|6|7|8|9)\d{9}$/
    if (!phone) { this.setData({ phonefocus: true }); return false }
    if (!regMobile.test(phone)) {
      wx.showToast({ title: '手机号错误', duration: 3000, icon: 'none' });
      this.setData({ phonefocus: true });
      return false;
    }
    //验证手机号 END
    if (!code) { this.setData({ codefocus: true }); return false }
    if (!password) { this.setData({ passwordfocus: true }); return false }
    console.log(code)
    //验证手机号 END
    wx.showLoading();
    wx.request({
      header: {
        'content-type': 'application/json',
        'content-type': 'application/x-www-form-urlencoded',
        'Authorization': app.Getopenid()
      },
      method: 'POST',
      url: app.DOMAIN_NAME +'/wechat/wechatUser/checkMobile',
      data: {
        mobile: phone,
        code: code
      },
      success: res => {
        wx.hideLoading()
        wx.setStorageSync('mobile', phone)
        wx.navigateTo({
          url: '/pages/information/information',
        })
      }
    })
    // app.AjaxRequest({})
  }
})