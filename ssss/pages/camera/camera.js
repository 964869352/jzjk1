Page({
  data: {
    getCodeTime:6
  },
  onLoad: function (option) {
    var that = this
    that.ctx = wx.createCameraContext()
    const eventChannel = that.getOpenerEventChannel()
    // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
    eventChannel.on('acceptDataFromOpenerPage', function (data) {
      that.setData({ index: data.index })
      if (data.index==0){
        that.setData({ sname: 'front'})
      } else if (data.index == 1) {
        that.setData({ sname: 'back' })
      } else if (data.index == 2) {
        that.setData({ sname: 'left' })
      } else if (data.index == 3) {
        that.setData({ sname: 'right' })
      } else if (data.index == 4) {
        that.setData({ sname: 'backside'})
      }
    })
  },
  upload: function (e) {
    // wx.chooseVideo({
    //   sourceType: ['album'],
    //   compressed: true,
    //   maxDuration: 10,
    //   camera: 'back',
    //   success: res => {
    //     this.setData({
    //       src: res.tempFilePath,
    //       hi: 555,
    //       video: 333
    //     })
    //   }
    // })
    wx.chooseImage({
      sourceType: ['album'],
      success: res => {
        this.setData({
          src: res.tempFilePaths[0],
          hi: 555,
          himge: 555
        })
      }
    })
  },
  onUnload() {
    wx.stopDeviceMotionListening()
  },
  onDeviceMotionChange: function (res) {
    this.setData({
      X: parseInt(res.beta),
      Y: parseInt(res.gamma),
      Z: parseInt(res.alpha),
      x: res.beta,
      y: parseInt(res.gamma)
    })
  },
  //拍摄照片
  btnClick3: function () {
    this.ctx.takePhoto({
      quality: 'high',
      success: (res) => {
        this.setData({
          src: res.tempImagePath,
          hi: 555,
          himge: 555
        })
      }
    })
  },
  rephotograph: function () {
    this.setData({
      hi: 0
    })
    if (this.data.index == 0) {
      that.setData({ sname: 'front' })
    } else if (this.data.index == 1) {
      that.setData({ sname: 'back' })
    } else if (this.data.index == 2) {
      that.setData({ sname: 'left' })
    } else if (this.data.index == 3) {
      that.setData({ sname: 'right' })
    } else if (this.data.index == 4) {
      that.setData({ sname: 'backside' })
    }
  },
  next: function (e) {
    const eventChannel = this.getOpenerEventChannel()
    eventChannel.emit('acceptDataFromOpenedPage', {
      src: this.data.src,
      index: this.data.index
    });
    var img_url = wx.getStorageSync('img_url');
    img_url[this.data.index] = this.data.src
    wx.setStorageSync('img_url', img_url)
    console.log(wx.getStorageSync('img_url'))
    eventChannel.off({})
    wx.navigateBack({})
  },
  back:res=>{
    wx.navigateBack({})
  },


  /**
   * 长按按钮 - 录像
   */
  handleLongPress: function (e) {
    // 长按方法触发，调用开始录像方法
    this.Countdown();
    this.startShootVideo();
  },

  /**
   * 开始录像的方法
   */
  startShootVideo() {
    this.ctx.startRecord({
      success: (res) => {
        wx.showToast({
          title: '录制开始',
        })
      },
      fail() {
      }
    })
  },

  /**
   * 结束录像
   */
  stopShootVideo() {
    this.setData({
      himge: 333
    })
    this.ctx.stopRecord({
      success: (res) => {
        wx.hideLoading();
        wx.showToast({
          title: '录制结束',
        })
        this.setData({
          src: res.tempVideoPath,
          hi:555,
          video:333
        })
      },
      fail() {
        wx.hideLoading();
      }
    })
  },

  //touch start 手指触摸开始
  handleTouchStart: function (e) {
    this.startTime = e.timeStamp;
  },

  //touch end 手指触摸结束
  handleTouchEnd: function (e) {
    this.endTime = e.timeStamp;
    //判断是点击还是长按 点击不做任何事件，长按 触发结束录像
    if (this.endTime - this.startTime > 350) {
      //长按操作 调用结束录像方法
      this.stopShootVideo();
    }
  },
  Countdown: function () {
    var that = this;
    var getCodeTime = that.data.getCodeTime;
    var intervalId = setInterval(function () {
      that.setData({ vi: 444 })
      wx.showToast({
        icon: 'none',
        title: that.data.verifyCodeTime,
      })
      getCodeTime--;
      that.setData({
        verifyCodeTime: getCodeTime + 's后结束录制'
      });
      //倒计时完成
      if (getCodeTime == 0) {
        clearInterval(intervalId);
        that.stopShootVideo()
        setTimeout(function () {
          that.setData({ vi: 555 })
        },1000)
      }
    }, 1000);
  },
})