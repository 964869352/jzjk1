var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    relation: ['爱人', '父亲', '母亲', '爷爷', '奶奶', '儿子', '女儿', '其他'],
    sex: ['男', '女'],
    // sexindex: 1,
    // relationindex: 2
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
      wx.setNavigationBarTitle({
        title: '个人信息'
      })
    
  },
  // 关系选择
  relationChange: function(e) {
    this.setData({
      relationindex: e.detail.value,
      h: 1
    })
  },
  // 关系选择
  sexChange: function (e) {
    this.setData({
      sexindex: e.detail.value,
      sexh: 1
    })
  },

  binfoSubmit: function(e) {
    var name = e.detail.value.name,
      idCard = e.detail.value.id,
      type = e.detail.value.relation,
      school = e.detail.value.school,
      classes = e.detail.value.classname,
      age = e.detail.value.age,
      sex = e.detail.value.sex
    if (!name) {
      this.setData({
        namefocus: true
      });
      return false
    };
    if (!sex) {
      wx.showToast({
        title: '请选择性别',
        icon: "none"
      });
      return false
    };
    if (!idCard) {
      this.setData({
        idfocus: true
      });
      return false
    };
    if (!age) {
      this.setData({
        agefocus: true
      });
      return false
    };
    if (!type) {
      wx.showToast({
        title: '请选择与本人的关系',
        icon: "none"
      });
      return false
    };
    if (!school) {
      this.setData({
        schoolfocus: true
      });
      return false
    };
    if (!classes) {
      this.setData({
        classnamefocus: true
      });
      return false
    };
    
    wx.showLoading();
    wx.request({
      header: {
        'content-type': 'application/json',
        'Authorization': app.Getopenid()
      },
      method: 'POST',
      url: app.DOMAIN_NAME +'/wechat/wechatFamily/saveSelfInfo',
      data: {
        name, type, idCard, school, classes, age, sex
      },
      success: res => {
        if (res.data.status == 10000) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '身份已过期,需重新授权',
            success: res => {
              if (res.confirm) {
                wx.navigateTo({
                  url: '/pages/login/login',
                })
              }
            }
          });
        } else if (res.data.status != 200 || !res.data.status) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '服务器开小差去了，请重试',
            showCancel: false
          });
        } else if (res.data.status == 200) {
          wx.hideLoading()
          // wx.navigateTo({
          //   url: '/pages/uploading/uploading?userid=' + res.data.data.id,
          // })
          wx.navigateTo({
            url: '/pages/takephotos/takephotos?userid=' + res.data.data.id,
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})