const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    report: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var token = wx.getStorageSync('token');
    //发送请求
    wx.showLoading();
    wx.request({
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'content-type': 'application/json',
        'Authorization': app.Getopenid()
      },
      method: 'POST',
      url: app.DOMAIN_NAME +'/wechat/analysis/listByUserId',
      success: res => {
        if (res.data.status == 10000) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '身份已过期,需重新授权',
            success: res => {
              if (res.confirm) {
                wx.navigateTo({
                  url: '/pages/login/login',
                })
              }
            }
          });
        } else if (res.data.status != 200 || !res.data.status) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '服务器开小差去了，请重试',
            showCancel: false
          });
        } else if (res.data.status == 200) {
          wx.hideLoading()
          that.setData({
            report: res.data.data
          })
        }
      }
    })
  },
  navto:function(e){
    if (e.currentTarget.dataset.t == "PARSED"){
      wx.navigateTo({
        url: '/pages/testResults/testResults?quantity=' + e.currentTarget.dataset.quantity + '&score=' + e.currentTarget.dataset.score,
      })
    }
  }
})