var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    finish: 1,
    selected: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(e) {
    console.log(e)
    this.setData({
      userid: e.userid
    })
  },
  // 关系选择
  // relationChange: function(e) {
  //   this.setData({
  //     relationindex: e.detail.value,
  //     h: 1
  //   })
  // },

  navg:function(){
    wx.redirectTo({
      url: '/pages/information/information',
    })
  },

  backhome: function () {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  

  binfoSubmit: function(e) {
    var that = this,
        height = e.detail.value.stature,
        weight = e.detail.value.weight,
        habit = e.detail.value.habit,
        medicalHistory = e.detail.value.medical,
        allergic = e.detail.value.allergy
    if (!height) {
      that.setData({
        staturefocus: true
      });
      return false
    };
    if (!weight) {
      that.setData({
        weightfocus: true
      });
      return false
    };
    if (!habit) {
      that.setData({
        habitfocus: true
      });
      return false
    };
    if (!medicalHistory) {
      that.setData({
        medicalfocus: true
      });
      return false
    };
    if (!allergic) {
      that.setData({
        allergyfocus: true
      });
      return false
    };
    if (that.data.selected == 0) {
      wx.showModal({
        title: '请先勾选',
        content: '《信息条款》',
        confirmText: '好的',
        showCancel: false
      })
      return false;
    };

    wx.showLoading();
    wx.request({
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'content-type': 'application/json',
        'Authorization': app.Getopenid()
      },
      method: 'POST',
      url: app.DOMAIN_NAME +'/wechat/wechatFamily/update',
      data: {
        height, weight, habit, medicalHistory, allergic,id:that.data.userid
      },
      success: res => {
        if (res.data.status == 10000) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '身份已过期,需重新授权',
            success: res => {
              if (res.confirm) {
                wx.navigateTo({
                  url: '/pages/login/login',
                })
              }
            }
          });
        } else if (res.data.status != 200 || !res.data.status) {
          wx.hideLoading()
          wx.showModal({
            confirmText: '好的',
            content: res.data.errmsg || '服务器开小差去了，请重试',
            showCancel: false
          });
        } else if (res.data.status == 200) {
          wx.hideLoading()
          that.setData({
            finish: 0
          })
        }
      }
    })
  },

  /**
   * 我已阅读协议
   */
  readAgreement: function() {
    this.setData({
      selected: this.data.selected == 0 ? 1 : 0,
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})