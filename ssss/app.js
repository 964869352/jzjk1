App({
  APPID: 'wx441cd94f3b99468d',
  DOMAIN_NAME: 'https://aabbcc.thinkine.com/', //接口域名  线上：https://aabbcc.thinkine.com
  globals: {
    refreshMyPages: false,
    homedata: {},
    editPro: {
      projectId: 0, //项目ID
      isedit: 0, //是否编辑
      json: {} //项目编辑后的详细数据
    },
    trainBeginCity: ''
  },
 
  onLaunch: function (res) {
    var that = this
    var memberInfo = wx.getStorageSync('MemberInfo')
    if (!memberInfo){ 
      wx.reLaunch({
        url: '/pages/login/login',
      })
      return false;
    };
    // 登录1111111
    wx.login({
      success: res => {
        wx.request({
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST',
          url: that.DOMAIN_NAME+'/social_user_login/login',
          data: {
            code: res.code,
            rawData: memberInfo.rawData,
            encryptedData: memberInfo.encryptedData,
            ivStr: memberInfo.iv,
            signature: memberInfo.signature
          },
          success: function (ret) {
            wx.setStorageSync('familyInfo', ret.data.data.familyInfo)
            if (ret.data.status != 200 || !ret.data.status) {
              wx.showModal({
                confirmText: '好的',
                content: ret.data.errmsg || '服务器开小差去了，请重试',
                showCancel: false
              });
            } else if (ret.data.status == 200) {
              wx.hideLoading()
            }
            console.log(ret.data)
          },
          fail: function (ret) { }
        })
      }
    })
  },
  ///////////////////////////////////////////////////////////////////////////
  //获取当前用户的openid
  Getopenid: function () {
    var token = wx.getStorageSync('token');
    return token;
  },


  //统一请求封装
  //mandate 接口值
  //data 传值，已自动转换为JSON
  //call 请求成功后回调的方法
  AjaxRequest: function (url, data, call) {
    var token = wx.getStorageSync('token');
      wx.showLoading({
        mask: true,
        title: '正在加载'
      });
    var that = this;
    //发送请求
    wx.request({
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'Authorization': token
      },
      method: 'POST',
      url: that.DOMAIN_NAME+url,
      data: {
        APPID: that.APPID,
        data: JSON.stringify(data)
      },
      success: function (ret) {
        wx.hideLoading();
        // if (ret.data.status != 200 || !ret.data.status) {
        //   wx.showModal({
        //     confirmText: '好的',
        //     content: ret.data.errmsg || '服务器开小差去了，请重试',
        //     showCancel: false
        //   });
        // } else if (ret.data.status == 200) {
        //   if (call) call(ret.data);
        // }
        if (call) call(ret.data);
      },
      fail: function (ret) {
        wx.hideLoading();
        wx.showModal({
          confirmText: '再试一次',
          content: '无法连接网络',
          showCancel: false,
          success: function () {
            that.AjaxRequest(mandate, data, call);
          }
        });
      }
    })
  },
})